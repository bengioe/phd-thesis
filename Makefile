
F = phd-thesis
_chapters = $(shell ls chapters)
chapters = $(addprefix chapters/,$(_chapters))

default: all

loop:
	while inotifywait -e close_write $(F).tex $(chapters); do make all; done

loop_with_errors:
	while inotifywait -q -o /dev/null -e close_write $(F).tex $(chapters); do make -s see_errors; done

none:


all:
	pdflatex -interaction=nonstopmode $(F)
	bibtex $(F)
	pdflatex -interaction=nonstopmode $(F)
	pdflatex -interaction=nonstopmode $(F)

compile_line = pdflatex -interaction=nonstopmode -file-line-error
grep_cmd = | grep -A 2 -i ".*:[0-9]*:.*" || true

see_errors:
	@echo "make"
	if [ -f $(F).aux ]; then rm $(F).aux; fi
	$(compile_line) $(F) $(grep_cmd)
	bibtex $(F) | grep -i "warning" || true
	$(compile_line) $(F) $(grep_cmd)
	$(compile_line) $(F) $(grep_cmd)


some:
	pdflatex  $(F)
	bibtex $(F)
	pdflatex  $(F)
	pdflatex  $(F)

rmtemp:
	rm $(F).aux $(F).bbl $(F).bcf $(F).blg $(F).idx $(F).log $(F).out $(F).run.xml $(F).toc

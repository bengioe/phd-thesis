
We will first describe what can be done when this correspondence is bijective, i.e. an $x$ is uniquely described by some sequence of $a_i$. Then, we will generalize our method to cases when correspondence is surjective, i.e. when multiple sequences describe the same $x$. For example, and of particular interest here, molecules can be described as graphs, which can be described in multiple orders, left-to-right, top-to-bottom, etc (other canonical representations such as SMILES strings also suffer from this problem).

\subsection{SumTrees}
[do we need the sum tree part? Probably not? idk]

SumTrees are a tree data structure that allow for efficient $O(\log_2 n)$ sampling from a set of $n$ elements $x$ with an associated score $R(x)>0$ such that $p(x) = R(x)/\sum_{x'}R(x')$. This is typically done by setting each $x$ to be the leaf of a binary tree of height $\lceil\log_2 n\rceil$, setting the score of the leaves to be $R(x)$, and setting the score of a non-leaf node to be the sum of the score of its descendants. Sampling is then done recursively; a non-leaf node compares the sum-of-scores of its left child to that of its right child to choose which child to choose. Generally, this can be done with any $k$-ary partial tree.

More formally, let $S(node)$ be the scalar value associated with some node, we write that 
\begin{equation}
    S(node) = R(node\;|\;\mbox{isleaf}(node)) + \sum_{c\in {\rm children}(node)} S(c)
\end{equation}
At a given non-leaf node, if $S$ has been computed correctly everywhere, we could sample child $c$ with probability
\begin{equation}
    P(c|node) = \frac{S(c)}{\sum_{c'\in {\rm children}(node)} S(c')} =  \frac{S(c)}{S(node)}
\end{equation}
until a leaf is reached. Let $(root, n_1, n_2, .., n_h)$ be the sequence of nodes leading to some leaf node $n_h$ associated with element $x$ (i.e. there is a bijection between the state and the corresponding action sequence). Then if we sample children as above, we have that:
\begin{equation}
    P(x) = \frac{S(n_1)}{S(root)}\frac{S(n_2)}{S(n_1)}\frac{S(n_3)}{S(n_2)}...\frac{R(x)}{S(n_h)} = \frac{R(x)}{S(root)}
\end{equation}

We can equivalently describe this data structure as a finite, deterministic MDP, and note the following. First, the quantity $S$ being estimated is akin to the value function $V^u$ of the uniform policy $\pi_u$:
\begin{equation}
    V^u(s) = \hat R(s) + \sum_{s'\in children(s)} \pi_u(.|s) V^u(s')
\end{equation}
Note that for $V^u(s)=S(node)$ to be truly equivalent, we need to modify $R(s)$ to account for the multiplicative $\pi_u(.|s)$. This can be done via a simple formula, but, now aware of this equivalence, let us work directly with the unweighted sum. Let's write
\begin{equation}
    V(s) = R(s) + \sum_{s'\in children(s)} V(s')
\end{equation}
and the associated action-value function:
\begin{equation}
    Q(s,a) = R(s') + V(s') = R(s') + \sum_{a'} Q(s',a')
\end{equation}
Note that this equation is exact, and that since the transition operator is deterministic in our setting, we directly write $(s,a,s')$ when unambiguous.

Having defined the pseudo-action-value of the uniform policy, we can recover $P(x)$ as defined above. Consider the bijective scenario between action sequences and states, then the probability of a trajectory $\tau$ is the product of probabilities of some policy $\pi$ going from the root to some sampled leaf, 
\begin{equation}
    p_\theta(\tau) = \prod_t \pi(a_t|s_t)
\end{equation}
which we've defined (in the definition of $P$) as the ratio of the sum of a child to the sum of a parent, or as the ratio of values of $s'$ to $s$,
\begin{equation}
    p_\theta(\tau) = \prod_{t=0}^{H-1}\frac{V(s_{t+1})}{V(s_t)}.
\end{equation}
Equivalently, the value of a child $s'$ is the action-value of the action $a$ in the parent $s$, and the value of $s$ is the sum over possible actions of the action values of $s$,
\begin{equation}
    p_\theta(\tau)= \prod_{t=0}^{H-1}\frac{Q(s_t,a_t)}{\sum_{a'} Q(s_t,a')}
\end{equation}
As in the definition of $P(x)$, the intermediary values cancel out ($R$ is zero in non-leaves) to yield
\begin{equation}
   p_\theta(\tau) = \frac{R(s_H)}{V(s_0=root)=Z}
\end{equation}
Thus, we have that the policy of interest can be expressed as:
\begin{equation}
    \pi(a|s) = \frac{Q(s,a)}{\sum_{a'} Q(s,a')}
\end{equation}

Note that the $Q$ above is really (equivalent to) $Q^u$ and not $Q^\pi$, which would be an entirely different quantity.

Finally, in classical SumTree data structures, the topology of the tree can be arbitrary, the order of nodes and leaves in the binary tree does not matter (except perhaps for numerical stability). Let's instead enforce a semantic significance to nodes and their children. In particular, let the root represent the empty sequence, let each non-leaf node $s$ have $|{\cal A}(s)|$ children representing the concatenation of the node's sequence with some $a\in\cal{A}(s)$, and let leaf nodes represent elements of $\cal X$ to which a unique sequence $(a_1, a_2, ..., a_h)$ is associated. When $s$ is a leaf node we can use $s$ and $x$ interchangeably.

Having associated a SumTree structure with an MDP, we propose to approximate $Q$ with the following learning TD-like objective (or rather, this closely resembles Expected SARSA, since we are evaluating the uniform policy using a sum over actions):
\begin{equation}
    \mathcal{L} = (Q_\theta(s,a) - R(s')- \sum_{a'} Q_\theta(s',a') )^2
\end{equation}

In practice, this objective in inconvenient for several reasons, and we instead use its $\log$-space equivalent:

\begin{equation}
    \mathcal{L} = [Q^{\log}_\theta(s,a)- \log (R(s') + \sum_{a'} \exp Q^{\log}_\theta(s',a')) ]^2
\end{equation}

This is ideal for two reasons, first, for numerical stability as the quantities estimated grow large, and second, this means that learning treats equally small and large errors in linear-space, meaning that learning is just as important in leaves (where $Q$ is relatively small) as it is in nodes near the root (where $Q$ gets large due to aggregation). This would not be the case normally due to the squaring of the loss, making smaller differences less relevant.

While this method is not the focus of this paper, and only intended to be didactic, we test it on some small toy problems, where it performs admirably well compared to similar methods estimating $p(x) \approx R(x)/Z$ (see appendix?).


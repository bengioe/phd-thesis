Exploration in RL

#### Count-Based Exploration with Neural Density Models
https://arxiv.org/abs/1703.01310

#### Unifying Count-Based Exploration and Intrinsic Motivation
https://arxiv.org/abs/1606.01868


#### Variational Information Maximisation for Intrinsically Motivated Reinforcement Learning
https://arxiv.org/abs/1509.08731

#### Improving policy gradient by exploring under-appreciated rewards
https://openreview.net/pdf?id=ryT4pvqll